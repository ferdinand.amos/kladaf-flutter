import 'package:flutter/material.dart';

class WeeklySchedule {
  final String activity;
  final String hour;
  final String detail;
  final String message;

  const WeeklySchedule({
    required this.activity,
    required this.hour,
    required this.detail,
    required this.message,
  });
}
