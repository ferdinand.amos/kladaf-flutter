import 'package:flutter/material.dart';

class Diary {
  final String id;
  final String title;
  final String content;
  final String dateCreated;

  const Diary({
    required this.id,
    required this.title,
    required this.content,
    required this.dateCreated,
  });
}
