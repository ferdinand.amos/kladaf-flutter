import 'package:flutter/material.dart';

class DiaryItem extends StatelessWidget {
  final String id;
  final String title;
  final String content;
  final String dateCreated;

  DiaryItem(
    this.id,
    this.title,
    this.content,
    this.dateCreated,
  );

  // void selectCategory(BuildContext ctx) {
  //   Navigator.of(ctx).pushNamed(
  //     CategoryMealsScreen.routeName,
  //     arguments: {
  //       'id': id,
  //       'title': title,
  //     },
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      // onTap: () => selectCategory(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Text(
          title,
          style: Theme.of(context).textTheme.headline6,
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Color.fromARGB(255, 224, 225, 221)),
      ),
      
    );
  }
}
