import 'package:flutter/material.dart';
import 'package:kladafmobile/screens/home.dart';
import 'package:kladafmobile/screens/progresstracker_taskscreen.dart';
import 'package:kladafmobile/screens/weeklyschedule_mainbody.dart';
import 'package:kladafmobile/screens/wishlist_home.dart';
import '../screens/weeklyschedule_main.dart';
import '../screens/progresstracker_home.dart';
import '../screens/podomorotimer_screen.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 80,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            margin: const EdgeInsets.only(top: 40.0),
            alignment: Alignment.centerLeft,
            color: Color(0xff343434),
            child: Text(
              'Kladaf',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Color(0xFFe0e1dd)),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          // Home Page
          ElevatedButton.icon(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Home()),
              );
            },
            icon: Icon(
              Icons.home,
              size: 26,
            ),
            label: Text(
              'Home',
              style: TextStyle(
                fontFamily: 'Segoe UI',
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            style: ElevatedButton.styleFrom(
              primary: Color(0xff343434),
            ),
          ),

          // Diary
          ElevatedButton.icon(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const WeeklyScheduleHome()),
              );
            },
            icon: Icon(
              Icons.border_color,
              size: 26,
            ),
            label: Text(
              'Diary',
              style: TextStyle(
                fontFamily: 'Segoe UI',
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            style: ElevatedButton.styleFrom(
              primary: Color(0xff343434),
            ),
          ),
          // Podomoro Timer
          ElevatedButton.icon(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const PodomoroTimer()),
              );
            },
            icon: Icon(
              Icons.timer,
              size: 26,
            ),
            label: Text(
              'Podomoro Timer',
              style: TextStyle(
                fontFamily: 'Segoe UI',
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            style: ElevatedButton.styleFrom(
              primary: Color(0xff343434),
            ),
          ),
          // Weekly Schedule
          ElevatedButton.icon(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const DailyScreen()),
              );
            },
            icon: Icon(
              Icons.date_range,
              size: 26,
            ),
            label: Text(
              'Weekly Schedule',
              style: TextStyle(
                fontFamily: 'Segoe UI',
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            style: ElevatedButton.styleFrom(
              primary: Color(0xff343434),
            ),
          ),
          // Progress Tracker
          ElevatedButton.icon(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const TaskScreen()),
              );
            },
            icon: Icon(
              Icons.inventory,
              size: 26,
            ),
            label: Text(
              'Progress Tracker',
              style: TextStyle(
                fontFamily: 'Segoe UI',
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            style: ElevatedButton.styleFrom(
              primary: Color(0xff343434),
            ),
          ),
          // Wish List
          ElevatedButton.icon(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const WishListApp()),
              );
            },
            icon: Icon(
              Icons.fact_check,
              size: 26,
            ),
            label: Text(
              'Wish List',
              style: TextStyle(
                fontFamily: 'Segoe UI',
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            style: ElevatedButton.styleFrom(
              primary: Color(0xff343434),
            ),
          ),
        ],
      ),
    );
  }
}
