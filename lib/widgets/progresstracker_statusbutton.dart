import 'package:flutter/material.dart';
import 'package:kladafmobile/screens/progresstracker_tasklist.dart';
import '../screens/progresstracker_constants.dart';

class StatusButton extends StatelessWidget {
  final stat;
  StatusButton({this.stat});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          print(statusMap[stat]);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => TaskList()),
          );
        },
        child: Card(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          elevation: 1.9,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              color: Color(0xFF778da9),
            ),
            padding: EdgeInsets.all(5.0),
            child: Center(
              child: Text(stat,
                  style: TextStyle(color: Color(0xFFe0e1dd), fontSize: 16.0)),
            ),
            height: 65.0,
            width: 120.0,
          ),
        ));
  }
}
