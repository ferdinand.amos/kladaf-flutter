import 'dart:convert';
import 'dart:core';
import 'package:flutter/material.dart';
import '../models/wishlist.dart';
import 'package:http/http.dart' as http;

class WishListProvider with ChangeNotifier {
  WishListProvider() {
    this.fetchTasks();
  }

  List<WishList> _items = [];

  List<WishList> get items {
    return [..._items];
  }

  List<WishList> _notComplete = [];

  List<WishList> get notComplete {
    return [..._notComplete];
  }

  List<WishList> _complete = [];

  List<WishList> get complete {
    return [..._complete];
  }

  fetchTasks() async {
    final String url = 'https://kladaf-app.herokuapp.com/wish-list/apis?format=json';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = json.decode(response.body) as List;
      _items = data.map<WishList>((json) => WishList.fromJson(json)).toList();
      notifyListeners();
    }
    _notComplete = notCompleteItem(_items);
    _complete = completeItem(_items);
  }

  //filtering wishlist not complete
  List<WishList> notCompleteItem (List<WishList> e){
    List<WishList> update = [];
    for (WishList element in e){
      if(element.complete == false){
        update.add(element);
      }
    }
    return update;
  }

  //filtering wishlist complete
  List<WishList> completeItem (List<WishList> e){
    List<WishList> update = [];
    for (WishList element in e){
      if(element.complete == true){
        update.add(element);
      }
    }
    return update;
  }

  //add wishlist
  void addWishList(WishList wish) async{
    final String url = 'https://kladaf-app.herokuapp.com/wish-list/apis';
    final response = await http.post(Uri.parse(url),
        headers: {'Content-Type':'application/json'},
        body: json.encode(wish));
    if (response.statusCode == 201){
      wish.id = json.decode(response.body)['id'];
      _items.add(wish);
      notifyListeners();
    }
    _notComplete = notCompleteItem(_items);
    _complete = completeItem(_items);
  }

  void completeWishList(WishList wish) async {
    final String url = 'https://kladaf-app.herokuapp.com/wish-list/apis/${wish.id}';
    final response = await http.delete(Uri.parse(url));

    if (response.statusCode == 204) {
      final WishList wishNew = WishList(id: wish.id,text: wish.text, complete: true);
      _items.remove(wish);
      _notComplete.remove(wish);
      addWishList(wishNew);
      notifyListeners();
    }
    _notComplete = notCompleteItem(_items);
    _complete = completeItem(_items);
  }
}