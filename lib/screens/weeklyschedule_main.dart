import 'package:flutter/material.dart';
import '../widgets/navbar.dart';
import '../screens/weeklyschedule_body.dart';

class WeeklyScheduleHome extends StatefulWidget {
  const WeeklyScheduleHome({Key? key}) : super(key: key);

  @override
  State<WeeklyScheduleHome> createState() => _HomeDartState();
}

class _HomeDartState extends State<WeeklyScheduleHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Weekly Schedule',
          style: TextStyle(
            color: Color(0xFFe0e1dd),
            fontFamily: 'Segoe UI',
          ),
        ),
        backgroundColor: Color(0xff343434),
      ),
      drawer: MainDrawer(),
      body: build(context),
    );
  }
}
