import 'package:kladafmobile/api/wishlist_api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class WishListHome extends StatelessWidget {
  const WishListHome({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => WishListProvider(),
      child: const MaterialApp(
        title: 'Flutter Demo',
        home: WishListPage(),
      ),
    );
  }
}

class WishListPage extends StatefulWidget {
  const WishListPage({Key? key}) : super(key: key);

  @override
  _WishListPageState createState() => _WishListPageState();
}

class _WishListPageState extends State<WishListPage> {
  Widget build(BuildContext context) {
    final wishP = Provider.of<WishListProvider>(context);

    return MaterialApp(
        home: Scaffold(
          backgroundColor: Color(0xFF0D1B2A),
          appBar: AppBar(
            title: Text(
              'Wish List', style: TextStyle(color: Color(0xFF0D1B2A), fontSize: 30),
            ),
            backgroundColor: Colors.white,
            centerTitle: true,
            iconTheme: IconThemeData(color: Color(0xFF0D1B2A)),
          ),
          drawer: Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: const <Widget>[
                DrawerHeader(
                  decoration: BoxDecoration(
                    color: Color(0xFF0D1B2A),
                  ),
                  child: Text(
                    'Kladaf App', style: TextStyle(color: Colors.blueGrey, fontSize: 24),
                  ),
                ),
                ListTile(
                  title: Text('Weekly Schedule'),
                ),
                ListTile(
                  title: Text('Progress Tracker'),
                ),
                ListTile(
                  title: Text(
                    'Wish List', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                  ),
                ),
                ListTile(
                  title: Text('Podomoro Timer'),
                ),
                ListTile(
                  title: Text('Diary'),
                ),
                ListTile(
                  title: Text('Rating Kladaf App'),
                ),
              ],
            ),
          ),
          //bodyyy ya nanti
          body: ListView.builder(
            shrinkWrap: true,
            itemCount: wishP.notComplete.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                trailing: IconButton(
                  icon: Icon(Icons.check, color: Colors.white,),
                  onPressed: () {
                    wishP.completeWishList(wishP.notComplete[index]);
                  },
                ),
                title: Text(
                  wishP.notComplete[index].text,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
                ),
              );
            },
          ),
        ));
  }
}