import 'package:flutter/material.dart';
import 'package:kladafmobile/screens/progresstracker_taskscreen.dart';
import '../widgets/navbar.dart';
import 'progresstracker_form.dart';

class ProgressTrackerHome extends StatefulWidget {
  const ProgressTrackerHome({Key? key}) : super(key: key);

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<ProgressTrackerHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Progress Tracker',
          style: TextStyle(color: Color(0xFFe0e1dd)),
        ),
        backgroundColor: Color(0xFF0d1b2a),
      ),
      drawer: MainDrawer(),
      body: TaskScreen(),
    );
  }
}
