import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kladafmobile/screens/progresstracker_taskscreen.dart';
import 'package:kladafmobile/widgets/navbar.dart';
import 'progresstracker_home.dart';

class FormPage extends StatefulWidget {
  const FormPage({Key? key}) : super(key: key);

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<FormPage> {
  TextEditingController taskController = TextEditingController();
  TextEditingController statusController = TextEditingController();

  List<String> tasks = [
    "No Status",
    "Not Started",
    "In Progress",
    "In Review",
    "Completed"
  ];
  String item = "No Status";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: MainDrawer(),
        body: Container(
            color: Color(0xff0d1b2a),
            child: Padding(
                padding: EdgeInsets.all(10),
                child: ListView(children: <Widget>[
                  Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.fromLTRB(15, 30, 15, 10),
                      child: Text(
                        'Task',
                        style: TextStyle(
                            fontFamily: 'Segoe UI',
                            color: Color(0xFFe0e1dd),
                            fontSize: 20),
                      )),
                  Container(
                      padding: EdgeInsets.fromLTRB(70, 5, 70, 5),
                      child: TextField(
                        controller: taskController,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Color(0xFF748ba5),
                            contentPadding: EdgeInsets.all(1.0),
                            border: OutlineInputBorder(),
                            labelText: 'Add Your Task here',
                            labelStyle: TextStyle(
                              fontFamily: 'Segoe UI',
                              color: Color(0xFFadb5bd),
                            )),
                      )),
                  Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.fromLTRB(15, 60, 15, 10),
                      child: Text(
                        'Status',
                        style: TextStyle(
                            fontFamily: 'Segoe UI',
                            color: Color(0xFFe0e1dd),
                            fontSize: 20),
                      )),
                  Container(
                      padding: EdgeInsets.fromLTRB(70, 5, 70, 5),
                      child: Column(
                        children: <Widget>[
                          DropdownButtonFormField(
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Color(0xFF748ba5),
                                contentPadding: EdgeInsets.all(1.0),
                                border: OutlineInputBorder(),
                                labelStyle: TextStyle(
                                  fontFamily: 'Segoe UI',
                                  color: Color(0xFFadb5bd),
                                )),
                            value: item,
                            items: tasks.map((String val) {
                              return DropdownMenuItem(
                                value: val,
                                child: Row(
                                  children: <Widget>[Text(val)],
                                ),
                              );
                            }).toList(),
                            onChanged: (String? newValue) {
                              setState(() {
                                item = newValue!;
                              });
                            },
                          )
                        ],
                      )),
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                    child: Center(
                        child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        onPrimary: Colors.white,
                        primary: Color(0xFF748ba5),
                      ),
                      child: Text('Add'),
                      onPressed: () {
                        final snackBar = SnackBar(
                          content: taskController.text.length == 0
                              ? Text('Fill The Blank Space!')
                              : Text('Succes!'),
                          action: SnackBarAction(
                            label: 'Undo',
                            onPressed: () {
                              // Some code to undo the change.
                            },
                          ),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        print(taskController.text);
                        print(item);
                        if (taskController.text.length != 0) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TaskScreen()),
                          );
                        }
                      },
                    )),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child: Center(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          onPrimary: Colors.white,
                          primary: Color(0xFF748ba5),
                        ),
                        child: Text('Back'),
                        onPressed: () {
                          Navigator.pop(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => TaskScreen()));
                        },
                      ),
                    ),
                  ),
                ]))));
  }
}
