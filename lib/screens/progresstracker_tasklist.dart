import 'package:flutter/material.dart';
import 'package:kladafmobile/screens/progresstracker_taskscreen.dart';
import '../widgets/progresstracker_statusbutton.dart';
import '../widgets/navbar.dart';
import 'progresstracker_form.dart';
import 'progresstracker_constants.dart';

class TaskList extends StatefulWidget {
  const TaskList({Key? key}) : super(key: key);

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<TaskList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xff0d1b2a),
        body: SafeArea(
            child: Row(children: <Widget>[
          Container(
            child: Container(
              padding: EdgeInsets.fromLTRB(8, 8, 0, 0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  onPrimary: Colors.white,
                  primary: Color(0xFF748ba5),
                ),
                child: Text('Back'),
                onPressed: () {
                  Navigator.pop(context,
                      MaterialPageRoute(builder: (context) => TaskScreen()));
                },
              ),
            ),
          )
        ])));
  }
}
