import 'dart:async';

import 'package:flutter/material.dart';

<<<<<<< HEAD
// void main() {
//   runApp(const MyApp());
// }
=======
void main() {
  runApp(const PodomoroTimer());
}
>>>>>>> 103fb11f9b2d65d86f2cde69de6fc0b9b4a29145

class PodomoroTimer extends StatelessWidget {
  const PodomoroTimer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Podomoro Timer',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        backgroundColor: const Color(0xFF37474F),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: const MyHomePage(title: 'Kladaf App Podomoro Timer'),
    );

  }
}

class MyHomePage extends StatefulWidget {

  final String title;

  const MyHomePage({Key? key, required this.title}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();

}


class _MyHomePageState extends State<MyHomePage> {
  late Timer timer;
  int minutes25 = 1500;
  int minutes5 = 300;
  int startPoint = 1500;

  int initialTime = 1500;

  int paused = 0;
  int reset_5 = 0;
  int reset_25 = 0;
  int reset = 0;


  get validator => null;


  String formatHHMMSS(int seconds) {
    int hours = (seconds / 3600).truncate();
    seconds = (seconds % 3600).truncate();
    int minutes = (seconds / 60).truncate();

    String hoursStr = (hours).toString().padLeft(2, '0');
    String minutesStr = (minutes).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');

    if (seconds < 1) {
      return "Podomoro completed";
    }
    if (hours == 0) {
      return "$minutesStr:$secondsStr";
    }
    return "$hoursStr:$minutesStr:$secondsStr";
  }

  void startTimer() {
    const oneSec = Duration(seconds: 1);
    timer = Timer.periodic(oneSec, (timer) {
      setState(() {
        if (startPoint < 1) {
          paused = 1;
          startPoint = initialTime;
        } else {
          if (paused == 0) {
            startPoint = startPoint - 1;
          }
          if (reset_25 == 1) {
            startPoint = minutes25;
            reset_25 = 0;
            initialTime = minutes25;
          }
          if (reset_5 == 1) {
            startPoint = minutes5;
            reset_5 = 0;
            initialTime = minutes5;
          }
          if (reset == 1) {
            startPoint = minutes25;
            reset = 0;
            initialTime = minutes25;
          }
        }
      });
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    //var validator;
    return Scaffold(
      body: Container(
        //constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(color: Color(0xFF37474F)),
        child: Center(

          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              const Expanded(
                flex: 10,
                child: FittedBox(
                  fit: BoxFit.fitWidth,
                  child: Text(
                    "Podomoro Timer",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: "Sedan",
                        fontSize: 30),
                  ),
                ),
              ),



              Expanded(
                flex: 10,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[

                    Expanded(
                        flex: 10,
                        child: button("Pomodoro", reset_25, 250, 90, 0)
                    ),

                    Expanded(
                        flex: 10,
                        child: button("Short Break", reset_5, 250, 90, 0)
                    ),

                    Expanded(
                        flex: 10,
                        child: button("Long Break", reset, 250, 90, 0)
                    ),
                    // Expanded(
                    //   flex: 1,
                    //   child: Container(),
                    // ),
                  ],
                ),
              ),

              Expanded(
                  flex: 15,
                  child: FittedBox(
                    fit: BoxFit.fitWidth,
                    child: Text(
                      formatHHMMSS(startPoint),
                      style: const TextStyle(
                          color: Colors.white,
                          fontFamily: "Helvetica Neue",
                          fontWeight: FontWeight.w100,
                          fontSize: 50),
                    ),
                  )),

              Expanded(
                  flex: 20,
                  child: button("Pause", paused, 200, 80, 50)
              ),
              // Expanded(
              //     flex: 10,
              //     child: button("Reset", reset, 250, 90, 50)
              // ),

              Form(
                key: _formKey,
                child: SizedBox(
                  width: 300,
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        autofocus: true,
                        decoration: const InputDecoration(
                            hintText: "I am working on...",
                            labelText: "Enter Your Task",
                            icon: Icon(Icons.task)),
                      ),
                    ],
                  ),
                ),
              ),

              //Spacer(),
            ],
          ),
        ),
      ),
    );
  }


  Widget button(String text, int status, double button_width,
      double button_height, double radius) {
    if (status == 0) {
      return unpressedButton(text, button_width, button_height, radius);
    } else {
      return pressedButton(text, button_width, button_height, radius);
    }
  }

  Widget unpressedButton(
      String text, double buttonWidth, double buttonHeight, double radius) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: buttonWidth,
          height: buttonHeight,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(radius)),
              color: const Color.fromARGB(255, 41, 37, 69),
              boxShadow: const [
                BoxShadow(
                  spreadRadius: -2,
                  color: Color.fromARGB(255, 29, 25, 51),
                  offset: Offset(7, 7),
                  blurRadius: 15,
                )
              ]),
        ),
        Container(
          width: buttonWidth,
          height: buttonHeight,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(radius)),
              color: const Color.fromARGB(255, 41, 37, 69),
              boxShadow: const [
                BoxShadow(
                  spreadRadius: -20,
                  color: Colors.white,
                  offset: Offset(-5, -5),
                  blurRadius: 15,
                )
              ]),
        ),
        MaterialButton(
          hoverColor: Colors.transparent,
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          child: Container(
              width: buttonWidth,
              height: buttonHeight,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(radius))),
              child: Container(
                  alignment: Alignment.center,
                  child: FittedBox(
                      fit: BoxFit.fitWidth,
                      child: Text(
                        text,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          color: Colors.white,
                          fontFamily: "Helvetica Neue",
                          fontWeight: FontWeight.w200,
                          fontSize: 40,
                        ),
                      )))),
          onPressed: () {
            setState(() {
              if (text == "Pause") {
                if (paused == 1) {
                  paused = 0;
                } else {
                  paused = 1;
                }
              }
              if (text == "Pomodoro") {
                reset_25 = 1;
              }
              if (text == "Short Break") {
                reset_5 = 1;
              }
              if (text == "Reset") {
                reset = 1;
              }
            });
          },
        )
      ],
    );
  }

  Widget pressedButton(
      String text, double buttonWidth, double buttonHeight, double radius) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: buttonWidth,
          height: buttonHeight,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(radius)),
              color: const Color.fromARGB(255, 41, 37, 69),
              boxShadow: const [
                BoxShadow(
                  spreadRadius: -12,
                  color: Colors.white,
                  offset: Offset(5, 5),
                  blurRadius: 15,
                )
              ]),
        ),
        Container(
          width: buttonWidth,
          height: buttonHeight,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(radius)),
              color: const Color.fromARGB(255, 41, 37, 69),
              boxShadow: const [
                BoxShadow(
                  spreadRadius: -2,
                  color: Color.fromARGB(255, 29, 25, 51),
                  offset: Offset(-7, -7),
                  blurRadius: 15,
                )
              ]),
        ),
        MaterialButton(
            hoverColor: Colors.transparent,
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            child: Container(
                width: buttonWidth,
                height: buttonHeight,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(radius))),
                child: Container(
                    alignment: Alignment.center,
                    child: FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          text,
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            color: Colors.white,
                            fontFamily: "Helvetica Neue",
                            fontWeight: FontWeight.w200,
                            fontSize: 40,
                          ),
                        )))),
            onPressed: () {
              setState(() {
                if (text == "Pause") {
                  if (paused == 1) {
                    paused = 0;
                  } else {
                    paused = 1;
                  }
                }
              });
            })
      ],
    );
  }
}

