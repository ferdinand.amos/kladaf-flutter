import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../dummy_diary.dart';
import '../widgets/diary_item.dart';

class DiariesScreen extends StatelessWidget {
  DiariesScreen({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();
  String? inputTitle = '';
  String? inputContent = '';
  String inputDateCreated = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 27, 38, 59),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: GridView(
          children: dummyDiaryData
              .map(
                (catData) => DiaryItem(
                  catData.id,
                  catData.title,
                  catData.content,
                  catData.dateCreated,
                ),
              )
              .toList(),
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: const Color.fromARGB(255, 65, 90, 119),
        foregroundColor: Color.fromARGB(255, 224, 225, 221),
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  content: Stack(
                    clipBehavior: Clip.none,
                    children: <Widget>[
                      Form(
                        key: _formKey,
                        child: Container(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: 'Title',
                                  icon: const Icon(Icons.people),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                ),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Please Enter The Title';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  setState(() {
                                    inputTitle = value;
                                  });
                                },
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: 'Content',
                                  icon: const Icon(Icons.lock),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                ),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Please Enter The Content';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  setState(() {
                                    inputContent = value;
                                  });
                                },
                              ),
                              RaisedButton(
                                child: const Text(
                                  "Submit",
                                  style: TextStyle(color: Colors.white),
                                ),
                                color: Colors.blue,
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    _formKey.currentState!.save();
                                    var now = DateTime.now();
                                    var formatter = DateFormat('yyyy-MM-dd');
                                    inputDateCreated = formatter.format(now);
                                  }
                                },
                              ),
                              RaisedButton(
                                child: const Text(
                                  "Cancel",
                                  style: TextStyle(color: Colors.white),
                                ),
                                color: Colors.red,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                );
              });
        },
        icon: Icon(Icons.add),
        label: Text('Diary'),
      ),
    );
  }
}
