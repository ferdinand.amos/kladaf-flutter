import 'package:flutter/material.dart';
import '../widgets/navbar.dart';
import 'weeklyschedule_body.dart';
import 'weeklyschedule_constants.dart';

class FetchData extends StatefulWidget {
  const FetchData({Key? key}) : super(key: key);

  @override
  _MainDataState createState() => _MainDataState();
}

class _MainDataState extends State<FetchData> {
  var boolList = <bool>[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: MainDrawer(),
        body: Container(
            color: Color(0xff0d1b2a),
            child: Padding(
                padding: EdgeInsets.fromLTRB(30, 60, 30, 20),
                child: ListView(children: <Widget>[
                  Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10),
                      child: Column(children: <Widget>[
                        for (var i = 0; i < 10; i++)
                          Card(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0))),
                            elevation: 1.9,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                                color: Color(0xFF778da9),
                              ),
                              padding: EdgeInsets.all(5.0),
                              height: 175.0,
                              width: 350.0,
                            ),
                          )
                      ])),
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                    child: Center(
                        child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        onPrimary: Colors.white,
                        primary: Color(0xFF415a77),
                      ),
                      child: Text('Back'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    )),
                  ),
                ]))));
  }
}
